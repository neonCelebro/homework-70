import React from 'react';
import {View} from 'react-native';

import Display from "./components/display/Display";
import Buttons from "./components/Buttons/Buttons";
import style from './style.css';

export default class App extends React.Component {
    state = {
        userWrite: '',
    }
    getNumbers = (num) => {
        this.setState({userWrite: this.state.userWrite.concat(num)})
    };
    delNumbers = () => {
        this.setState({userWrite: this.state.userWrite.slice(0, -1)})
    };
    getResult = () => {
        const userWrite = this.state.userWrite;
        try {
            const result = eval(userWrite).toString();
            this.setState({userWrite: result});
        } catch (e) {
            this.setState({userWrite})
        }
    };

    render() {
        return (
                <View className={style.container}>
                    <Display
                        display={this.state.userWrite}
                    />
                    <Buttons del={this.delNumbers} getResult={this.getResult} cliked={this.getNumbers}/>
                </View>
        );
    }
}
