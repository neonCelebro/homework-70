import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import buttons from './buttons.css';

export default class Buttons extends React.Component {

    render(){
        const buttonValues = ['1','2','3','4','5','6','7','8','9','0','*','/','+','-','(',')'];
        return (
            <View className={buttons.container}>
                {buttonValues.map((btn, id) => {
                  return <TouchableOpacity
                      onPress={() => this.props.cliked(btn)}
                      key={id} className={buttons.style}>
                      <Text className={buttons.text}>{btn}</Text>
                  </TouchableOpacity>
                })}
                <TouchableOpacity onPress={this.props.getResult} className={buttons.blue}>
                    <Text className={buttons.text}>=</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.props.del} className={buttons.red}>
                    <Text className={buttons.text}>del</Text>
                </TouchableOpacity>
                </View>
        );
    }
}

