import React from 'react';
import {Text, View} from 'react-native';
import display from './display.css';

export default class Display extends React.Component {
    render(){
        return (
            <View className={display.input}>
                <Text className={display.text}>{this.props.display}</Text>
            </View>
        );
    }
};
